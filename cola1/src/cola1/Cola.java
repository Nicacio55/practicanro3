/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cola1;

/**
 *
 * @author  EVERT LUIS NICACIO PACHECO CI:7450101
 */
import java.util.*;
public class Cola {
    public static void main( String args[] ){
       Scanner leer = new Scanner(System.in);
      
       colagenerica obj = new colagenerica();
      
       int op;
       int num;
      
       do{
          menu();
          op = leer.nextInt();
         
          switch(op){
              case 1:
                     System.out.println( "Numero a insertar" );
                     num = leer.nextInt();
                     if(obj.inscola(num)){
                        System.out.println( "fre"+obj.fre+"fin"+obj.fin+"aux"+obj.max );
                        System.out.println( "El numero "+num+" se inserto en la cola ["+obj.dret+"]" );
                        System.out.println();
                     }
                     else{
                          System.out.println( "Cola llena" );
                     }
                     break;
              case 2:
                    if(obj.retcola()){
                       System.out.println( "El dato retirado fue: "+obj.dret );
                    }
                    else{
                        System.out.println( "Cola vacia" );
                    }
                    break;
              case 3:
                    if(obj.fre==-1 && obj.fin==-1){
                       System.out.println( "Cola vacia" );
                    }
                    else{
                         System.out.println( "Estado de la cola:" );
                         for(int i=obj.fre; i<=obj.fin; i++){
                            System.out.print(obj.c[i]+" \t");
                         }
                         break;
                    }
          }
       }
       while(op != 4);
    }
   
    public static void menu(){    
       System.out.println( "\t Menu para colas \n" );
       System.out.println( "1.- Añadir" );
       System.out.println( "2.- Borrar" );
       System.out.println( "3.- Mostrar" );
       System.out.println( "4.- Salir" );
       System.out.println( "\n Seleccione" );
    }
}


class colagenerica
 {
      public int max;
      protected Object dret;
      public Object c[];
      public int fre = -1;
      public int fin = -1;
     
      public colagenerica()
       {
            max=20;
            c=new Object [max];
       }
     
      public colagenerica(int n)
         { max=n;
          c=new Object [max];
         }
     
      public boolean colallena(int fin,int max)
      {
      boolean llena;
      if (fin==max-1)
        llena=true;
        else
          llena=false;
      return llena;
     }
    
    public boolean colavacia(int fre)
      {
      boolean vacia;
      if (fre==-1)
        vacia=true;
        else
          vacia=false;
      return vacia;
     }  
       
      public boolean inscola(Object dato)
       {
            if (fin==max-1)
               return false;
            fin++;
            c[fin] = dato;
            if (fin==0)
               fre=0;
            return true;
     }
       
      public boolean retcola()
        {
            if (fre ==-1)
               return false;
            dret=c[fre];
            if (fre==fin)
              {
                  fre=-1;
                  fin=-1;
              }
             else
               fre++;
            return true;
        }
 }
